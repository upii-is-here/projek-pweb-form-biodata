function submitForm() {
    var nama = document.getElementById("nama").value;
    var nim = document.getElementById("nim").value;
    var umur = document.getElementById("umur").value;
    var gender = document.getElementById("gender").value;
    var hobi = document.getElementById("hobi").value;
    var asal = document.getElementById("asal").value;
    var email = document.getElementById("email").value;
    var password = document.getElementById("password").value;

    var namaError = document.getElementById("nama-error");
    var nimError = document.getElementById("nim-error");
    var umurError = document.getElementById("umur-error");
    var genderError = document.getElementById("gender-error");
    var hobiError = document.getElementById("hobi-error");
    var asalError = document.getElementById("asal-error");
    var emailError = document.getElementById("email-error");
    var passwordError = document.getElementById("password-error");
    
    namaError.textContent = "";
    nimError.textContent = "";
    umurError.textContent = "";
    genderError.textContent = "";
    hobiError.textContent = "";
    asalError.textContent = "";
    emailError.textContent = "";
    passwordError.textContent = "";

    let isValid = true;

    if (nama === "" || nama.length < 2) {
        namaError.textContent = "Masukkan nama minimal 2 karakter!";
        isValid = false;
    }
    
    if (nim === "" || nim.length !== 10) {
        nimError.textContent = "Masukkan NIM dengan 10 karakter!";
        isValid = false;
    }
    
    if (umur === "" || umur <= 0) {
        umurError.textContent = "Masukkan umur lebih dari 0!";
        isValid = false;
    }

    if (gender === "") {
        genderError.textContent = "Pilih jenis kelamin Anda!";
        isValid = false;
    }

    if (hobi === "") {
        hobiError.textContent = "Masukkan hobi Anda!";
        isValid = false;
    }

    if (asal === "") {
        asalError.textContent = "Masukkan asal Anda!";
        isValid = false;
    }

    if (email === "" || !email.includes("@")) {
        emailError.textContent = "Masukkan alamat email yang valid!";
        isValid = false;
    }

    if (password === "" || password.length < 6) {
        passwordError.textContent = "Masukkan password minimal 6 karakter!";
        isValid = false;
    }

    if (isValid ) {
        alert("Data Anda sudah benar");
    }

    return isValid;
}